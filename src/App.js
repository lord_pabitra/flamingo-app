import { useState, useCallback } from 'react';

import './App.css';
import './assets/css/common.css';

import Topbar from './components/topbar/topbar';
// import PreviewHeader from './components/preview-header/preview-header';
import HeaderLogo from './components/header-logo/header-logo';
import PbrHead from './components/pbr-masthead/pbr-masthead';
import SiteContent from './components/site-content/site-content';
import Services from './components/services/services';
import MidSection from './components/mid-section/mid-section';
import Agents from './components/agents/agents';
import Pricings from './components/pricings/pricings';
import Portfolios from './components/portfolios/portfolios';
import Testimonials from './components/clients-testimonials/testimonials';
import AllNews from './components/all-news/all-news';
import TalkToUs from './components/talk-to-us/talk-to-us';
import PreFooter from './components/pre-footer/pre-footer';
import Footer from './components/footer/footer';
import SideBar from './components/side-bar/side-bar';

function App() {

  const [isSideBarOpen, setIsSideBarOpen] = useState(false);
  const toggleSideBar = useCallback(() => {
    setIsSideBarOpen(isSideBarOpen => !isSideBarOpen);
  }, []);
  
  return (
    <div className="App">
      <Topbar toggleSideBar={toggleSideBar}/>
      <HeaderLogo />
      <SideBar isOpen={isSideBarOpen} toggleSideBar={toggleSideBar}/>
      <PbrHead />
      <SiteContent />
      <Services />
      <MidSection />
      <Agents />
      <Pricings />
      <Portfolios />
      <Testimonials />
      <AllNews />
      <TalkToUs />
      <PreFooter />
      <Footer />
    </div>
  );
}

export default App;
