import './portfolios.css';
import portfoliosResponse from './mock-response';
import Portfolio from '../portfolio/portfolio';

const Portfolios = () => {
    const resposneArr = portfoliosResponse;
    return (
        <div id="portfolios_container" className="portfolios-container">
            <div className="tite_info">
                <h3 className="heading">Our Cases</h3>
                <div className="subtitle">what we have done</div>
            </div>
            <div className="row portfolios-inner-container">
                {[...resposneArr].map((item, index) => <Portfolio key={index} data={item} />)}
            </div>
        </div>
    );
};

export default Portfolios;