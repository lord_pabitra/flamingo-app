const portfoliosResponse = [
    {
        heading: 'Drag and drop page builder',
        content: 'Passage of Lorem Ipsum of passages of Lorem Ipsum available, but the majity have suffered alteration in some form ...',
        image: {
            src: 'images/portfolios/portfolio1/portfolio1-870.jpg',
            srcSet:'images/portfolios/portfolio1/portfolio1-870.jpg 870w, images/portfolios/portfolio1/portfolio1-150x150.jpg 150w, images/portfolios/portfolio1/portfolio1-600x600.jpg 600w, images/portfolios/portfolio1/portfolio1-300x300.jpg 300w, images/portfolios/portfolio1/portfolio1-100x100.jpg 100w'
        }
    },
    {
        heading: 'Criminal Tax Fraud',
        content: 'Passage of Lorem Ipsum of passages of Lorem Ipsum available, but the majity have suffered alteration in some form ...',
        image: {
            src: 'images/portfolios/portfolio2/portfolio2-870.jpg',
            srcSet: 'images/portfolios/portfolio2/portfolio2-870.jpg 870w, images/portfolios/portfolio2/portfolio2-150x150.jpg 150w, images/portfolios/portfolio2/portfolio2-600wx600w.jpg 600w, images/portfolios/portfolio2/portfolio2-300x300.jpg 300w, images/portfolios/portfolio2/portfolio2-100x100.jpg 100w'
        }
    },
    {
        heading: 'Exhibit One. Seven. Seventeen.',
        content: 'Passage of Lorem Ipsum of passages of Lorem Ipsum available, but the majity have suffered alteration in some form ...',
        image: {
            src: 'images/portfolios/portfolio3/portfolio3-870.jpg',
            srcSet: 'images/portfolios/portfolio3/portfolio3-870.jpg 870w, images/portfolios/portfolio3/portfolio3-150x150.jpg 150w, images/portfolios/portfolio3/portfolio3-600wx600w.jpg 600w, images/portfolios/portfolio3/portfolio3-300x300.jpg 300w, images/portfolios/portfolio3/portfolio3-100x100.jpg 100w'
        }
    },
    {
        heading: 'Monday Vibes',
        content: 'Passage of Lorem Ipsum of passages of Lorem Ipsum available, but the majity have suffered alteration in some form ...',
        image: {
            src: 'images/portfolios/portfolio4/portfolio4-870.jpg',
            srcSet: 'images/portfolios/portfolio4/portfolio4-870.jpg 870w, images/portfolios/portfolio4/portfolio4-150x150.jpg 150w, images/portfolios/portfolio4/portfolio4-600wx600w.jpg 600w, images/portfolios/portfolio4/portfolio4-300x300.jpg 300w, images/portfolios/portfolio4/portfolio4-100x100.jpg 100w'
        }
    }
];

export default portfoliosResponse;