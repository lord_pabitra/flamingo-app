import './header-logo.css';
import logo from '../../assets/images/header-logo/logo.png';

const HeaderLogo = () => (
<div className="header-logo">
    <div className="header-logo-inner">
        <div className="header-middle">
            <div className="logo-wrapper">
                <div id="pbr-logo" className="logo logo-theme"> 
                <a href="http://demo2.themelexus.com/corpec/">
                    <img src={logo} alt="corpec" />
                </a>
            </div>
            </div>
            <div className="general-style d-none d-lg-flex">
                <div className="textwidget custom-html-widget">
                    <ul>
                        <li className="icon-office">
                            <h5>Certified Company</h5>
                            <span>ISO 9001:2005</span>
                        </li>
                        <li className="icon-cup">
                            <h5>The Best Industrial</h5>
                            <span>Solution Provider</span>
                        </li>
                    </ul>
                </div>
                <div className="download-button d-none">
                    <a href="#%20" className="btn btn-primary btn-radius-35">
                        <span className="flaticon-download-to-storage-drive"></span>
                        PURCHASE
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
);

export default HeaderLogo;