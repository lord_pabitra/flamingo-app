import './agents.css';
import agentsResponse from './mock-response';
import Agent from '../agent/agent';

const Agents = () => {
    const resposneArr = agentsResponse;
    return (
        <div id="agents_container" className="agents-container">
            <div className="tite_info">
                <h3 className="heading">Meet Our Agents</h3>
                <div className="subtitle">awesome people</div>
            </div>
            <div className="row agents-inner-container">
                {[...resposneArr].map((item, index) => <Agent key={index} data={item} />)}
            </div>
        </div>
    );
};

export default Agents;