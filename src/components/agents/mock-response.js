const agentsResponse = [
    {
        name: 'John Smithy',
        designation: 'Founder & CEO',
        intro: '"You\'re always changing, and so should your introduction."',
        profileImage: {
            src: 'images/agents/agent1/agent1-370.jpg',
            srcSet: 'images/agents/agent1/agent1-370.jpg 370w, images/agents/agent1/agent1-150x150.jpg 150w, images/agents/agent1/agent1-300x300.jpg 300w, images/agents/agent1/agent1-100x100.jpg 100w'
        }
    },
    {
        name: 'Linda Kloe',
        designation: 'Senior Engineer',
        intro: '"You\'re always changing, and so should your introduction."',
        profileImage: {
            src: 'images/agents/agent2/agent2-370.jpg',
            srcSet: 'images/agents/agent2/agent2-370.jpg 370w, images/agents/agent2/agent2-150x150.jpg 150w, images/agents/agent2/agent2-300x300.jpg 300w, images/agents/agent2/agent2-100x100.jpg 100w'
        }
    },
    {
        name: 'Stephen Pakan',
        designation: 'Senior Engineer',
        intro: '"You\'re always changing, and so should your introduction."',
        profileImage: {
            src: 'images/agents/agent3/agent3-370.jpg',
            srcSet: 'images/agents/agent3/agent3-370.jpg 370w, images/agents/agent3/agent3-150x150.jpg 150w, images/agents/agent3/agent3-300x300.jpg 300w, images/agents/agent3/agent3-100x100.jpg 100w'
        }
    }
];

export default agentsResponse;