import './footer.css';

const Footer = () => {
    return (
        <section className="footer">
			<div className="container">
				<div className="copyright-inner clearfix"> Copyright © 2021 corpec. All rights reserved.</div>
			</div>
			<a href="/#" className="scrollup"><span className="fa fa-long-arrow-up"></span></a>
		</section>
    );
};

export default Footer;