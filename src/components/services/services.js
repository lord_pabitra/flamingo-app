import './services.css';

import Service from '../service/service';
import servicesResponse from './mock-response';

const Services = () => {
    
    const resposneArr = servicesResponse;
    return (
        <div id="services_container" className="services-container">
            <div className="tite_info">
                <h3 className="heading">Services We Provide</h3>
                <div className="subtitle">Covered in these areas</div>
            </div>
            <div className="row services-inner-container">
                {[...resposneArr].map((item, index) => <Service key={index} data={item} />)}
            </div>
        </div>
    );
};

export default Services;