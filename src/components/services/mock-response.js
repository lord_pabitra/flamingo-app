const servicesResponse = [
    {
        heading: 'Intellectual Property',
        content: 'Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac',
        mainImage: {
            src: 'images/services/intellect/intellect-870.jpg',
            srcSet: 'images/services/intellect/intellect-870.jpg 870w, images/services/intellect/intellect-300x200.jpg 300w, images/services/intellect/intellect-768x512.jpg 768w, images/services/intellect/intellect-600x400.jpg 600w'
        },
        iconImage: {
            src: 'images/services/intellect/icon.png'
        }
    },
    {
        heading: 'Portfolio Management',
        content: 'Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac',
        mainImage: {
            src: 'images/services/portfolio/portfolio-870.jpg',
            srcSet: 'images/services/portfolio/portfolio-870.jpg 870w, images/services/portfolio/portfolio-300x200.jpg 300w, images/services/portfolio/portfolio-768x512.jpg 768w, images/services/portfolio/portfolio-600x400.jpg 600w'
        },
        iconImage: {
            src: 'images/services/portfolio/icon.png'
        }
    },
    {
        heading: 'Finance & Risk',
        content: 'Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac',
        mainImage: {
            src: 'images/services/finance/finance-870.jpg',
            srcSet: 'images/services/finance/finance-870.jpg 870w, images/services/finance/finance-300x200.jpg 300w, images/services/finance/finance-768x512.jpg 768w, images/services/finance/finance-600x400.jpg 600w'
        },
        iconImage: {
            src: 'images/services/finance/icon.png'
        }
    },
    {
        heading: 'Technology Advisory',
        content: 'Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac',
        mainImage: {
            src: 'images/services/technology/technology-870.jpg',
            srcSet: 'images/services/technology/technology-870.jpg 870w, images/services/technology/technology-300x200.jpg 300w, images/services/technology/technology-768x512.jpg 768w, images/services/technology/technology-600x400.jpg 600w'
        },
        iconImage: {
            src: 'images/services/technology/icon.png'
        }
    },
    {
        heading: 'Blockchain',
        content: 'Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac',
        mainImage: {
            src: 'images/services/blockchain/blockchain-870.jpg',
            srcSet: 'images/services/blockchain/blockchain-870.jpg 870w, images/services/blockchain/blockchain-300x200.jpg 300w, images/services/blockchain/blockchain-768x512.jpg 768w, images/services/blockchain/blockchain-600x400.jpg 600w'
        },
        iconImage: {
            src: 'images/services/blockchain/icon.png'
        }
    },
    {
        heading: 'Capital Market',
        content: 'Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac',
        mainImage: {
            src: 'images/services/capital/capital-870.jpg',
            srcSet: 'images/services/capital/capital-870.jpg 870w, images/services/capital/capital-300x200.jpg 300w, images/services/capital/capital-768x512.jpg 768w, images/services/capital/capital-600x400.jpg 600w'
        },
        iconImage: {
            src: 'images/services/capital/icon.png'
        }
    }
];

export default servicesResponse;