import './service.css';

const Service = ({index, data}) => (
<div className="col-12 col-md-4 first-child services">
    <article>
        <div className="service-wrapper grid_v1">
            <div className="service-box-image"> 
                <a href="/#" className="service-box-image-inner "> 
                    <img width="474" height="316" 
                        src={data.mainImage.src} 
                        className="attachment-large size-large wp-post-image" 
                        alt="" loading="lazy" 
                        srcSet={data.mainImage.srcSet} 
                        sizes="(max-width: 474px) 100vw, 474px" /> 
                </a>
            </div>
            <div className="box-wrapper">
                <div className="box-header">
                    <div className="service-box-icon"> 
                        <a href="/#" className="service-box-icon-inner "> 
                            <img src={data.iconImage.src} alt={data.heading} /> 
                        </a>
                    </div>
                    <h4>
                        <a href="/#" rel="bookmark">
                            {data.heading}
                        </a>
                    </h4>
                </div>
                <div className="entry-content">
                    <div className="service-content">
                        <div className="service-description">{data.content}</div>
                        <div className="service-readmore"> <a href="/#">view detail</a></div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
);

export default Service;