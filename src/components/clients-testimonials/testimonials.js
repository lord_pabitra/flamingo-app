import './testimonials.css';

const Testimonials = () => {
    return (
        <div className="testimonials-container">
			<div className="container-inner">
				<h1 className="widget-heading">Client’ Testimonials</h1>
				<div id="testimonial-carousel" className="carousel-container carousel slide" data-ride="carousel">
				  <div className="carousel-inner">
					<div className="carousel-item active">
						<div className="item">
						   <div className="testimonials-body">
							  <div className="testimonials-content">
								 <div className="testimonials-description">
									<p>They cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will.</p>
								 </div>
							  </div>
							  <div className="info-avatar">
								 <div className="testimonials-meta">
									<h5 className="testimonials-name"> <a href="/#">Leonardo dicaprio</a></h5>
									<div className="job">Designer</div>
								 </div>
							  </div>
						   </div>
						</div>
					</div>
					<div className="carousel-item">
						<div className="item">
							<div className="testimonials-body">
								<div className="testimonials-content">
									<div className="testimonials-description">
										<p>They cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will.</p>
									</div>
								</div>
								<div className="info-avatar">
									<div className="testimonials-meta">
										<h5 className="testimonials-name"> <a href="/#">Leonardo dicaprio</a></h5>
										<div className="job">Designer</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="carousel-item">
						<div className="item">
							<div className="testimonials-body">
								<div className="testimonials-content">
									<div className="testimonials-description">
										<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
									</div>
								</div>
								<div className="info-avatar">
									<div className="testimonials-meta">
										<h5 className="testimonials-name"> <a href="/#">Jane Doe</a></h5>
										<div className="job">CEO, PRESS MAG</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="carousel-item">
						<div className="item">
							<div className="testimonials-body">
								<div className="testimonials-content">
									<div className="testimonials-description">
										<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
									</div>
								</div>
								<div className="info-avatar">
									<div className="testimonials-meta">
										<h5 className="testimonials-name"> <a href="/#">Aenean</a></h5>
										<div className="job">CEO, PRESS MAG</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				  </div>
				  <div className="carousel-controls">
					<a className="carousel-control-prev carousel-control left" href="#testimonial-carousel" role="button" data-slide="prev">
						<span className="fa fa-long-arrow-left" aria-hidden="true"></span>
						<span className="sr-only">Previous</span>
					</a>
					<a className="carousel-control-next carousel-control right" href="#testimonial-carousel" role="button" data-slide="next">
						<span className="fa fa-long-arrow-right" aria-hidden="true"></span>
						<span className="sr-only">Next</span>
					</a>
				  </div>
				</div>
			</div>
		</div>
    );
};

export default Testimonials;