import './topbar.css';

const Topbar = ({toggleSideBar}) => (
<div className="topbar">
    <div className="topbar-container">
        <div id="pbr-topbar" className="pbr-topbar d-none d-lg-block">
            <div className="inner">
                <div className="topbar-left">
                    <ul>
                        <li> <i className="fa fa-phone" aria-hidden="true"></i> +32-456-789012</li>
                        <li> <i className="fa fa-paper-plane" aria-hidden="true"></i> 121 Park Drive, Varick Str, Newyork, USA</li>
                    </ul>
                </div>
                <div className="topbar-right">
                    <ul className=" social">
                        <li className="social-fb"> <a target="_blank" href="/#"><i className="fa fa-facebook"></i></a></li>
                        <li className="social-tw"> <a target="_blank" href="/#"><i className="fa fa-twitter"></i></a></li>
                        <li className="social-go"> <a target="_blank" href="/#"><i className="fa fa-google-plus"></i></a></li>
                        <li className="social-in"> <a target="_blank" href="/#"><i className="fa fa-instagram"></i></a></li>
                        <li className="social-pi"> <a target="_blank" href="/#"><i className="fa fa-pinterest"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div className="topbar-mobile d-flex d-lg-none">
            <div className="active-mobile"> 
                <button onClick={toggleSideBar} className="btn btn-offcanvas btn-toggle-canvas offcanvas" type="button"> 
                    <i className="fa fa-bars"></i> 
                </button>
            </div>
            <div className="topbar-inner d-flex">
                <div className="active-mobile search-popup">
                    <span className="fa fa-search"></span>
                    <div className="active-content">
                        <form method="get" className="searchform" action="http://demo2.themelexus.com/corpec/">
                            <div className="pbr-search input-group"> <input name="s" maxLength="40" className="form-control input-large input-search" type="text" size="20" placeholder="Search..."/> <span className="input-group-addon input-large btn-search"> <input type="submit" className="fa" value=""/> </span></div>
                        </form>
                    </div>
                </div>
                <div className="active-mobile setting-popup">
                    <span className="fa fa-user"></span>
                    <div className="active-content">
                        <div className="">
                            <div className="menu-account-menu-container">
                                <ul id="menu-account-menu" className="menu-topbar">
                                    <li id="menu-item-435" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-435"><a href="http://demo2.themelexus.com/corpec/checkout/">Checkout</a></li>
                                    <li id="menu-item-437" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-437"><a href="http://demo2.themelexus.com/corpec/my-account/">My account</a></li>
                                    <li id="menu-item-438" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-438"><a href="http://demo2.themelexus.com/corpec/cart/">Shopping Cart</a></li>
                                    <li id="menu-item-439" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-439"><a href="http://demo2.themelexus.com/corpec/wishlist/">Wishlist</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="active-mobile cart-popup">
                    <span className="fa fa-shopping-bag"></span>
                    <div className="active-content">
                        <div className="widget_shopping_cart_content">
                            <div className="cart_list ">
                                <div className="empty">No products in the cart.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
);

export default Topbar;