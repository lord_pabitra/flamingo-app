import MenuItem from './menu-item/menu-item';
import MenuItemsData from './data';
import './side-bar.css';

const SideBar = ({isOpen, toggleSideBar}) => {

    return (
        <div id="pbr-off-canvas" className={`pbr-off-canvas sidebar-offcanvas d-block d-lg-none ${isOpen && 'active'}`}>
            <div className="pbr-off-canvas-body">
                <div className="offcanvas-head bg-primary"> 
                    <button type="button" className="btn btn-offcanvas btn-toggle-canvas btn-default" onClick={toggleSideBar}> 
                        <i className="fa fa-close"></i> 
                    </button> 
                    <span class="ml-3">Menu</span>
                </div>
                <nav className="navbar-offcanvas" role="navigation">
                    <div className="navbar-collapse">
                        <ul id="main-menu-offcanvas" className="nav navbar-nav">
                            {[...MenuItemsData].map((itemData, index) => <MenuItem key={index} data={itemData} itemClicked={toggleSideBar}/>)}
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    );
};

export default SideBar;