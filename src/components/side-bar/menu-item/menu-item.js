import { useState, useCallback } from 'react';
import { Link } from 'react-scroll';

import './menu-item.css';

const MenuItem = ({data, itemClicked}) => {

    const [showChildItem, setShowChildItem] = useState(false);

    const caretLinkClicked = useCallback(() => {
        setShowChildItem(showChildItem => !showChildItem);
    }, []);

    return (
        <li className="sidebar-menu-item level-0">
            <Link href="/#" onClick={itemClicked} to={data.href} smooth={true} offset={-70} duration={500}>{data.label}</Link>
            { 
                (data.children && data.children.length) ? (
                    <>
                        <b className="caret" onClick={caretLinkClicked}></b>
                        <ul className={`dropdown-menu ${showChildItem ? 'active' : ''}`}>
                            {data.children.map((child, index) => (
                                <li key={index} className="level-1">
                                    <Link href="/#" onClick={itemClicked} to={child.href} smooth={true} offset={-70} duration={500}>{child.label}</Link>
                                </li>
                            ))}
                        </ul>
                    </>
                ) : null 
            }
        </li>
    );
};

export default MenuItem;