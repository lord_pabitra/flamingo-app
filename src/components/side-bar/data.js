const MenuItemsData = [
	{
		label: 'Home',
		href: '/#',
		children: [
			{
				label: 'Home1',
				href: '/#'
			},
			{
				label: 'Home2',
				href: '/#'
			}
		]
	},
	{
		label: 'Our Services',
		href: 'services_container',
		children: [
			{
				label: 'Service 01',
				href: 'services_container'
			},
			{
				label: 'Service 02',
				href: 'services_container'
			},
			{
				label: 'Service 03',
				href: 'services_container'
			}
		]
	},
	{
		label: 'Our Cases',
		href: 'portfolios_container',
		children: [
			{
				label: 'Portfolio 01',
				href: 'portfolios_container'
			},
			{
				label: 'Portfolio 02',
				href: 'portfolios_container'
			},
			{
				label: 'Portfolio 03',
				href: 'portfolios_container'
			}
		]
	},
	{
		label: 'Blog',
		href: 'all_news_container',
		children: [
			{
				label: 'Blog1',
				href: 'all_news_container'
			},
			{
				label: 'Blog2',
				href: 'all_news_container'
			},
			{
				label: 'Blog3',
				href: 'all_news_container'
			}
		]
	},
	{
		label: 'Shop',
		href: '/#',
		children: []
	}
];

export default MenuItemsData