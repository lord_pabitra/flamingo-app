import './agent.css';

const Agent = ({data}) => {
    return (
        <div className="col-md-4 col-sm-4 agent-container">
            <div className="team-v1 pbr-our-team">
                <div className="team-image zoom-2 space-20 team-header">
                    <a href="http://demo2.themelexus.com/corpec/team/john-smithy/" title={data.name}>
                        <img
                            width="370"
                            height="370"
                            src={data.profileImage.src}
                            className="attachment-widget size-widget wp-post-image"
                            alt=""
                            loading="lazy"
                            srcSet={data.profileImage.srcSet}
                            sizes="(max-width: 370px) 100vw, 370px"
                        />
                    </a>
                    <div className="team-info d-none">
                        <a className="btn btn-lg btn-primary" href="http://demo2.themelexus.com/corpec/team/john-smithy/">
                            View profile <i className="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div className="team-body team-footer">
                    <div className="team-body-content">
                        <h3 className="team-name">
                            <a href="http://demo2.themelexus.com/corpec/team/john-smithy/">{data.name}</a>
                        </h3>
                        <p>{data.designation}</p>
                        <p><i>{data.intro}</i></p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Agent;