import './preview-header.css';

const PreviewHeader = () => (
<div className="preview-header">
    <div className="preview__envato-logo">
        <a className="header-envato_market" href="https://themeforest.net/item/corpec-corporate-wordpress-theme/21746634">Envato Market</a>
    </div>
    <div id="js-preview__actions" className="preview__actions">
        <div className="preview__action--buy">
            <a className="header-buy-now" href="https://themeforest.net/checkout/from_item/21746634?license=regular&amp;support=bundle_6month">Buy now</a>
        </div>
    </div>
</div>
);

export default PreviewHeader;