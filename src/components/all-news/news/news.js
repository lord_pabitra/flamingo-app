import './news.css';

const News = ({data}) => {
    return (
        <div className="col-sm-4 col-md-4 col-lg-4 lastest_new">
			<article id="post-100" className="nice-style post-100 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-creativity category-startup tag-business tag-design">
				<div className="post-container">
					<div className="blog-post-detail blog-post-grid">
						<figure className="entry-thumb">
							<div className="post-thumbnail"> 
                                <img width="870" height="635" 
                                    src={data.image.src}
                                    className="attachment-corpec-full-width size-corpec-full-width wp-post-image" 
                                    alt="pictorial presentation of the news" 
                                    loading="lazy" 
                                    srcSet={data.image.srcSet}
                                    sizes="(max-width: 870px) 100vw, 870px"
                                />
                            </div>
							<span className="comments-link"> 
                                <a href="http://demo2.themelexus.com/corpec/2018/05/28/decisions-are-the-frequent-fabric-of-our-daily-design/#respond">
                                    {data.commentCount} comments
                                </a> 
                            </span>
						</figure>
						<div className="information-post text-left">
							<div className="entry-meta"> 
                                <span className="entry-date"> 
                                    <span className="fa fa-clock-o"></span> 
                                    <span className="year">&nbsp;{data.releaseDate}</span>
                                    <span className="entry-category"> by </span> 
                                    <span className="author">
                                        <a href="http://demo2.themelexus.com/corpec/author/admin/" title="Posts by admin" rel="author">{data.releasedBy}</a>
                                    </span> 
                                </span>
                            </div>
							<h3 className="entry-title mt-3"> 
                                <a href="http://demo2.themelexus.com/corpec/2018/05/28/decisions-are-the-frequent-fabric-of-our-daily-design/"> {data.heading} </a>
                            </h3>
							<div className="description">
								<p>{data.content}</p>
							</div>
						</div>
					</div>
				</div>
			</article>
		</div>
    );
};

export default News;