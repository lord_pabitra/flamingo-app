import './all-news.css';
import newsResponse from './mock-response';
import News from './news/news';

const AllNews = () => {
    const resposneArr = newsResponse;
    return (
        <div id="all_news_container" className="all-news-container">
            <div className="tite_info">
                <h3 className="heading">Latest News</h3>
                <div className="subtitle">READ ALL NEWS</div>
            </div>
            <div className="row all-news-inner-container">
                {[...resposneArr].map((item, index) => <News key={index} data={item} />)}
            </div>
        </div>
    );
};

export default AllNews;