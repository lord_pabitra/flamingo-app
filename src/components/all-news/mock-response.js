const newsResponse = [
    {
        heading: 'Decisions are the frequent  fabric of our daily design.',
        content: 'Passage of Lorem Ipsum of passages of Lorem Ipsum available, but the majity have suffered alteration in some form by injected andomised rds which don’t look even slightly believable If you are going.',
        commentCount: '0',
        releaseDate: 'May 28, 2018',
        releasedBy: 'ADMIN',
        image: {
            src: 'images/news/news1/news1-870.jpg',
            srcSet: 'images/news/news1/news1-870.jpg 870w, images/news/news1/news1-300x219.jpg 300w, images/news/news1/news1-768x561.jpg 768w, images/news/news1/news1-600x438.jpg 600w'
        }
    },
    {
        heading: 'Design is inherently optimistic. That is  its power.',
        content: 'An image of a laptop, iPad, Android phone, and iPhone containing the same image displayed at multiple sizes to',
        commentCount: '0',
        releaseDate: 'May 28, 2018',
        releasedBy: 'ADMIN',
        image: {
            src: 'images/news/news2/news2-870.jpg',
            srcSet: 'images/news/news2/news2-870.jpg 870w, images/news/news2/news2-300x219.jpg 300w, images/news/news2/news2-768x561.jpg 768w, images/news/news2/news2-600x438.jpg 600w'
        }
    },
    {
        heading: 'Falsely Accused Of Committing A Crime?',
        content: 'The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words. There are many variations of passages of Lorem Ipsum',
        commentCount: '0',
        releaseDate: 'May 28, 2018',
        releasedBy: 'ADMIN',
        image: {
            src: 'images/news/news3/news3-870.jpg',
            srcSet: 'images/news/news3/news3-870.jpg 870w, images/news/news3/news3-300x219.jpg 300w, images/news/news3/news3-768x561.jpg 768w, images/news/news3/news3-600x438.jpg 600w'
        }
    }
];

export default newsResponse;