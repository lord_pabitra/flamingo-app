import './pricing.css';

const Pricing = ({position, data}) => {
    return (
        <div className="col-12 col-sm-4 pricing-container mt-5 mt-md-0 p-0">
		    <div className="vc_column-inner">
			  <div className="wpb_wrapper">
				 <div className="pricing pricing-v1">
					<div className="pricing-header">
					    <div className="plan-price"> 
                            <sup className="plan-currency">$</sup> 
                            <span className="plan-figure">{data.amountPerMonth}</span> 
                            <span className="plan-date">mon</span>
                        </div>
					    <h4 className="plan-title">
                            <span>{data.planType}</span>
                        </h4>
					</div>
					<div className="pricing-body text-left">
					   <div className="plain-info">
						  <ul>
                             {[...data.offers].map((offer, index) => <li key={index}>{offer}</li>)}
						  </ul>
					   </div>
					</div>
					<div className="pricing-footer"> 
                        {(position%2 === 0) ? (
                            <a className="btn btn-sm btn-primary btn-block" href="/#">Buy it now</a>
                        ) : (
                            <a className="btn btn-sm btn-block" href="/#">Buy it now</a>
                        )}
                    </div>
				 </div>
			  </div>
		    </div>
		</div>
    );
};

export default Pricing;