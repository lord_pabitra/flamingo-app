import { useState, useEffect, useCallback } from 'react';

const TOTAL_ANIMATION_TIME_IN_SEC = 3;

const ClientCount = ({count, content}) => {

    const [clientCount, setClientCount] = useState(0);
    const [trigger, setTrigger] = useState(false);

    const handleScrollEvent = async () => {
        const element = await document.querySelector('.gama-section');
        const elementPosition = await element.getBoundingClientRect().top;
    
        if (window.pageYOffset > elementPosition) {
            setTrigger(true);
            unRegisterEvent();
        }
    };
    const registerEvent = useCallback(() => {
        document.addEventListener("scroll", handleScrollEvent);
    }, [])
    const unRegisterEvent = useCallback(() => {
        document.removeEventListener("scroll", handleScrollEvent);
    }, []);

    const increment = useCallback(() => {
        const sInt = setInterval(() => {
            setClientCount(clientCount => {
                if(clientCount < count) {
                    return clientCount+1;
                } else {
                    clearInterval(sInt);
                    return clientCount;
                }
            });
        }, (TOTAL_ANIMATION_TIME_IN_SEC * 1000/count));
    }, [count]);

    useEffect(() => {
        if (trigger) {
            increment();
        }
        registerEvent();
        return unRegisterEvent;
    }, [trigger, increment, registerEvent, unRegisterEvent]);

    return (
        <div className="client-count col-12 col-md-3 text-center text-lg-left">
            <h1 className="count">{clientCount}</h1>
            <p className="count-type">{content}</p>
        </div>
    );
};

export default ClientCount;