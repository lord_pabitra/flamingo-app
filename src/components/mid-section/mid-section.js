import { useMemo } from 'react';

import Modal from '../shared/modal/modal';
import ClientCount from './client-count/client-count';
import './mid-section.css';
import largeImg from '../../assets/images/mid-section/image-large.jpg';
import smallImg from '../../assets/images/mid-section/image-small.jpg';

const midSectionData = [
    {content: 'ALL TIME CLIENTS', count: 13},
    {content: 'CLIENTS IN THIS YEAR', count: 11},
    {content: 'NEW PROJECTS', count: 19},
    {content: 'AVG PROFIT INCREASED', count: 95}
];

const MidSection = ({data = midSectionData}) => {
    const srcSet = `${largeImg} 585w, ${smallImg} 300w`;
    const modalId = useMemo(() => 'read_more_modal', []);
    const modalHeader = useMemo(() => 'This is a dummy modal header', []);
    const modalBody = useMemo(() => <p>This is modal body</p>, []);

    return (
    <section className="mid-section container-fluid">
        <div className="alpha-section row">
            <div className="alpha-l-section col-12 col-md-6">
            
            </div>
            <div className="alpha-r-section col-12 col-md-6 px-0 px-md-3">
                <div className="alpha-r-section-inner pt-5 mb-5">
                    <div className="wpb_wrapper">
                        <div className="widget-text-heading  heading-style1 text-left">
                            <div className="tite_info d-flex flex-column-reverse">
                                <h3 className="widget-heading "> <span className="heading-text">We are trusted by more than 5,000 clients</span></h3>
                                <div className="subtitle"> Did you know that</div>
                            </div>
                        </div>
                        <div className="wpb_text_column wpb_content_element ">
                            <div className="wpb_wrapper">
                                <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. More than 25 years of experience working in the industry has enabled us to build our services and solutions in strategy, consulting, digital.</p>
                            </div>
                        </div>
                        <div className="vc_btn3-container vc_btn3-inline mt-5"> 
                            <a className="contact-us-btn" href="/#" title="" data-toggle="modal" data-target={'#' + modalId}>READ MORE</a>
                        </div>
                        <Modal id={modalId} header={modalHeader} body={modalBody}></Modal>
                    </div>
                </div>
            </div>
        </div>
        <div className="beta-section row d-none d-lg-flex">
            <div className="beta-l-section d-none d-md-block col-md-6 px-0 text-right">
                <img width="100%" height="450" src={largeImg} className="vc_single_image-img attachment-full" alt="" loading="lazy" srcSet={srcSet} sizes="(max-width: 585px) 100vw, 585px" />
            </div>
            <div className="beta-r-section d-none d-md-block col-md-6">
                <div className="content">
                    Our goal is to be at the heart of financial services industry as businesses expand across. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean….
                </div>
            </div>
        </div>
        <div className="gama-section row">
            {[...data].map((item, index) => <ClientCount key={index} count={item.count} content={item.content}></ClientCount>)}
        </div>
    </section>
    );
};

export default MidSection;