import './pbr-masthead.css';
import { Link } from "react-scroll";

const PbrHead = () => (
<header id="pbr-masthead" className="site-header pbr-header-default d-none d-md-block" role="banner">
    <div className="pbr-masthead-inner">
        <div className="header-main d-lg-flex">
            <div id="pbr-mainmenu" className="pbr-mainmenu navbar-mega-default">
                <nav className="pbr-megamenu slide animate navbar navbar-mega p-0" role="navigation">
                    <div className="collapse navbar-collapse navbar-mega-collapse">
                        <ul id="primary-menu" className="nav navbar-nav megamenu">
                            <li id="menu-item-205" className="dropdown active menu-item-205 level-0">
                                <a href="/#" className="dropdown-toggle">Home <b className="caret"></b></a>
                                <ul className="dropdown-menu">
                                    <li id="menu-item-206" className="menu-item-206 level-1"><a href="/#">Home 2</a></li>
                                    <li id="menu-item-207" className="menu-item-207 level-1"><a href="/#">Home 3</a></li>
                                    <li id="menu-item-208" className="menu-item-208 level-1"><a href="/#">Home 4</a></li>
                                    <li id="menu-item-209" className="menu-item-209 level-1"><a href="/#">Home 5</a></li>
                                </ul>
                            </li>
                            <li id="menu-item-210" className="dropdown menu-item-210 level-0">
                                <a href="/#" className="dropdown-toggle">Page <b className="caret"></b></a>
                                <ul className="dropdown-menu">
                                    <li id="menu-item-212" className="menu-item-212 level-1"><a href="/#">About Us</a></li>
                                    <li id="menu-item-213" className="menu-item-213 level-1"><a href="/#">About02</a></li>
                                    <li id="menu-item-214" className="menu-item-214 level-1"><a href="/#">About03</a></li>
                                    <li id="menu-item-215" className="menu-item-215 level-1"><Link href="/#" to="contact_us_section" smooth={true} offset={-70} duration={1700}>Contact 01</Link></li>
                                    <li id="menu-item-216" className="menu-item-216 level-1"><Link href="/#" to="contact_us_section" smooth={true} offset={-70} duration={1700}>Contact 02</Link></li>
                                    <li id="menu-item-217" className="menu-item-217 level-1"><Link href="/#" to="agents_container" smooth={true} offset={-70} duration={700}>Team 01</Link></li>
                                    <li id="menu-item-211" className="menu-item-211 level-1"><Link href="/#" to="agents_container" smooth={true} offset={-70} duration={700}>Team 02</Link></li>
                                </ul>
                            </li>
                            <li id="menu-item-412" className="dropdown menu-item-412 level-0">
                                <Link href="/#" to="services_container" smooth={true} offset={-70} duration={500} className="dropdown-toggle">Our Services <b className="caret"></b></Link>
                                <ul className="dropdown-menu">
                                    <li id="menu-item-279" className="menu-item-279 level-1"><Link href="/#" to="services_container" smooth={true} offset={-70} duration={500}>Service 01</Link></li>
                                    <li id="menu-item-280" className="menu-item-280 level-1"><Link href="/#" to="services_container" smooth={true} offset={-70} duration={500}>Service 02</Link></li>
                                    <li id="menu-item-413" className="menu-item-413 level-1"><Link href="/#" to="services_container" smooth={true} offset={-70} duration={500}>Service 03</Link></li>
                                </ul>
                            </li>
                            <li id="menu-item-221" className="dropdown menu-item-221 level-0">
                                <Link href="/#" to="portfolios_container" smooth={true} offset={-70} duration={1200} className="dropdown-toggle">Our Cases <b className="caret"></b></Link>
                                <ul className="dropdown-menu">
                                    <li id="menu-item-220" className="menu-item-220 level-1"><Link href="/#" to="portfolios_container" smooth={true} offset={-70} duration={1200}>Portfolio01</Link></li>
                                    <li id="menu-item-219" className="menu-item-219 level-1"><Link href="/#" to="portfolios_container" smooth={true} offset={-70} duration={1200}>Portfolio02</Link></li>
                                    <li id="menu-item-414" className="menu-item-414 level-1"><Link href="/#" to="portfolios_container" smooth={true} offset={-70} duration={1200}>Portfolio03</Link></li>
                                </ul>
                            </li>
                            <li id="menu-item-226" className="dropdown menu-item-226 level-0">
                                <Link href="/#" to="all_news_container" smooth={true} offset={-70} duration={1500} className="dropdown-toggle">Blog <b className="caret"></b></Link>
                                <ul className="dropdown-menu">
                                    <li id="menu-item-411" className="menu-item-411 level-1"><Link href="/#" to="all_news_container" smooth={true} offset={-70} duration={1500}>Blog 1</Link></li>
                                    <li id="menu-item-223" className="menu-item-223 level-1"><Link href="/#" to="all_news_container" smooth={true} offset={-70} duration={1500}>Blog 2</Link></li>
                                    <li id="menu-item-225" className="menu-item-225 level-1"><Link href="/#" to="all_news_container" smooth={true} offset={-70} duration={1500}>Blog 3</Link></li>
                                    <li id="menu-item-224" className="menu-item-224 level-1"><Link href="/#" to="all_news_container" smooth={true} offset={-70} duration={1500}>Blog 4</Link></li>
                                    <li id="menu-item-416" className="menu-item-416 level-1"><Link href="/#" to="all_news_container" smooth={true} offset={-70} duration={1500}>Blog 5</Link></li>
                                </ul>
                            </li>
                            <li id="menu-item-464" className="menu-item-464 level-0"><a href="/#">Shop</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div className="header-icon d-flex">
                <div className="pbr-header-right pull-right hidden-xs hidden-sm">
                    <div className="header-inner">
                        <div className="pull-right">
                            <div className="pbr-topcart">
                                <div id="cart" className="dropdown">
                                    <a className="dropdown-toggle mini-cart" data-toggle="dropdown" aria-expanded="true" role="button" aria-haspopup="true" data-delay="0" href="/#" title="View your shopping cart"> <span className="text-skin cart-icon"> <i className="fa fa-shopping-bag"></i> </span>  <span className="mini-cart-items"> 0 <em>item</em> </span>  <span className="woocommerce-Price-amount amount"><bdi><span className="woocommerce-Price-currencySymbol">$</span>0.00</bdi></span> </a>
                                    <div className="dropdown-menu">
                                        <div className="widget_shopping_cart_content">
                                            <div className="cart_list ">
                                                <div className="empty">No products in the cart.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="search-container" className="search-box-wrapper pull-right hidden-xs hidden-sm">
                    <div className="opal-dropdow-search dropdown">
                        <a data-target=".bs-search-modal-lg" href="/#" data-toggle="modal" className="btn btn-link search-focus dropdown-toggle dropdown-toggle-overlay"> <i className="fa fa-search"></i> </a>
                        <div className="modal fade bs-search-modal-lg" tabIndex="-1" role="dialog" aria-hidden="true">
                            <div className="modal-dialog modal-lg">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <button aria-label="Close" data-dismiss="modal" className="close btn btn-xs btn-primary pull-right" type="button"><span aria-hidden="true">x</span></button>
                                        <h4 id="gridSystemModalLabel" className="modal-title">Search</h4>
                                    </div>
                                    <div className="modal-body">
                                        <div className="toggle-overlay-container">
                                            <div className="search-box">
                                                <form method="get" className="input-group search-category" action="http://demo2.themelexus.com/corpec/">
                                                    <div className="input-group-addon search-category-container">
                                                        <div className="select">
                                                            <select name="product_cat" id="product_cat" className="dropdown_product_cat">
                                                                <option value="" selected="selected">Select a category</option>
                                                                <option className="level-0" value="audio">Audio&nbsp;&nbsp;(3)</option>
                                                                <option className="level-0" value="camera">Camera&nbsp;&nbsp;(5)</option>
                                                                <option className="level-0" value="electronics">Electronics&nbsp;&nbsp;(6)</option>
                                                                <option className="level-0" value="laptop">Laptop&nbsp;&nbsp;(1)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <input name="s" maxLength="60" className="form-control search-category-input" type="text" size="20" placeholder="What do you need..." />
                                                    <div className="input-group-btn"> 
                                                        <label className="btn btn-link btn-search"> 
                                                            <span className="title-search hidden">Search</span> 
                                                            <input type="submit" className="fa searchsubmit" value="" /> 
                                                        </label> 
                                                        <input type="hidden" name="post_type" value="product" />
                                                    </div>
                                                </form>
                                            </div>
                                            <div className="dropdown-toggle-button" data-target=".toggle-overlay-container"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
);

export default PbrHead;