import './pricings.css';
import pricingsResponse from './mock-response';
import Pricing from '../pricing/pricing';

const Pricings = () => {
    const resposneArr = pricingsResponse;
    return (
        <div className="pricings-container">
            <div className="tite_info">
                <h3 className="heading">Our Pricing Table</h3>
                <div className="subtitle">awesome people</div>
            </div>
            <div className="row pricings-inner-container">
                {[...resposneArr].map((item, index) => <Pricing key={index} position={index} data={item} />)}
            </div>
        </div>
    );
};

export default Pricings;