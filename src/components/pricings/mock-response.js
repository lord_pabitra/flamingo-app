const pricingsResponse = [
    {
        amountPerMonth: '45',
        planType: 'Basic',
        offers: [
            '2 Web Design',
            'Free hosting',
            'Social media integration',
            'Unlimited Data Storage',
            '50 GB Bandwidth',
            'Enhanced Security',
            'Free six months hosting'
        ]
    },
    {
        amountPerMonth: '75',
        planType: 'Standard',
        offers: [
            '2 Web Design',
            'Free hosting',
            'Social media integration',
            'Unlimited Data Storage',
            '50 GB Bandwidth',
            'Enhanced Security',
            'Free six months hosting'
        ]
    },
    {
        amountPerMonth: '95',
        planType: 'Premium',
        offers: [
            '2 Web Design',
            'Free hosting',
            'Social media integration',
            'Unlimited Data Storage',
            '50 GB Bandwidth',
            'Enhanced Security',
            'Free six months hosting'
        ]
    }
];

export default pricingsResponse;