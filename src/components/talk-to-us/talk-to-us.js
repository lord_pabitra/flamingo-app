import './talk-to-us.css';

const TalkToUs = () => {
    return (
        <div className="talk-to-us-container">
			<div className="container container-full">
				<div className="row">
					<div className="col-sm-9">
						<div className="vc_column-inner p-1">
							<div className="wpb_wrapper">
								<div className="widget-text-heading  heading-style3 text-left">
									<div className="tite_info">
										<h3 className="widget-heading "> <span className="heading-text">Wanna Talk To Us?</span></h3>
										<div className="subtitle"> Please feel free to contact us. We’re super happy to talk to you. Feel free to ask anything.</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="vc_column_container col-sm-3">
						<div className="vc_column-inner p-0">
							<div className="wpb_wrapper">
								<div className="vc_btn3-container vc_btn3-right"> 
									<button className="contact-us-btn">contact us</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    );
};

export default TalkToUs;