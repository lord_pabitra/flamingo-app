import { useEffect, useState } from 'react';

import './site-content.css';

const SiteContent = () => {

    const [itemHeight, setItemHeight] = useState('0');

    useEffect(() => {
        const calculateItemHeight = () => {
            const deviceWidth = document.documentElement.clientWidth;
            let carouselItemHeight = '0';

            if(deviceWidth > 1100) {
                carouselItemHeight = (deviceWidth / 2.35) + 'px';
            } else {
                carouselItemHeight = (deviceWidth / 1.64) + 'px';
            }
            setItemHeight(carouselItemHeight);
        };
        calculateItemHeight();
        window.addEventListener('resize', calculateItemHeight);

        return () => window.removeEventListener('resize', calculateItemHeight);
    }, []);

    return (
    <div className="site-container">
        <div>
            <div id="site-carousel" className="carousel-container carousel slide" data-ride="carousel">
                <div className="carousel-inner">
                    <div className="carousel-item carousel-item1 active" style={{'height': itemHeight}}>
                        <div className="item item1 d-flex">
                            {/*<img src={sliderImg1} alt="background" />*/}
                            <div className="testimonials-body d-none d-md-block">
                                <div className="testimonials-content">
                                    <div className="testimonials-description">
                                        <p>They cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will.</p>
                                    </div>
                                </div>
                                <div className="info-avatar">
                                    <div className="testimonials-meta">
                                    <h5 className="testimonials-name"> <a href="/#">Leonardo dicaprio</a></h5>
                                    <div className="job">Designer</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="carousel-item carousel-item2" style={{'height': itemHeight}}>
                        <div className="item item2 d-flex">
                            {/*<img src={sliderImg2} alt="background" />*/}
                            <div className="testimonials-body d-none d-md-block">
                                <div className="testimonials-content">
                                    <div className="testimonials-description">
                                        <p>They cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will.</p>
                                    </div>
                                </div>
                                <div className="info-avatar">
                                    <div className="testimonials-meta">
                                        <h5 className="testimonials-name"> <a href="/#">Leonardo dicaprio</a></h5>
                                        <div className="job">Designer</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="carousel-controls">
                    <a className="carousel-control-prev carousel-control left" href="#site-carousel" role="button" data-slide="prev">
                        <span className="fa fa-angle-left" aria-hidden="true"></span>
                    </a>
                    <a className="carousel-control-next carousel-control right" href="#site-carousel" role="button" data-slide="next">
                        <span className="fa fa-angle-right" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    );
};

export default SiteContent;