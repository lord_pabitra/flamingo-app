import './portfolio.css';

const Portfolio = ({data}) => {
    return (
        <div className="portfolio-entry portfolio-item col-lg-3 col-md-6 col-sm-6 p-0">
		   <div className="pbr-portfolio-content">
			  <div className="ih-item square colored effect16">
				 <div className="img"> 
                    <img width="870" height="870" 
                        src={data.image.src}
                        className="attachment-full size-full wp-post-image" 
                        alt="" 
                        loading="lazy" 
                        srcSet={data.image.srcSet}
                        sizes="(max-width: 870px) 100vw, 870px" 
                    />
                </div>
			  </div>
			  <div className="info-inner text-left">
				<h3>
                    <a className="text" href="http://demo2.themelexus.com/corpec/portfolio/drag-and-drop-page-builder-2/">{data.heading}</a>
                </h3>
				<p className="description">{data.content}</p>
				<h5 className="learmore">
                    <a href="http://demo2.themelexus.com/corpec/portfolio/drag-and-drop-page-builder-2/">
                        READ MORE<i className="fa fa-long-arrow-right" aria-hidden="true"></i>
                    </a>
                </h5>
			  </div>
		   </div>
		</div>
    );
};

export default Portfolio;