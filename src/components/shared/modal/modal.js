import { useMemo } from 'react';

const Modal = ({id, header, body, footer}) => {
    const modalId = id || 'flamingo_app_modal';
    const modalFooter = useMemo(() => {
        return footer ? footer : (
            <>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary">Understood</button>
            </>
        );
    }, [footer]);

    return (
        <div className="modal fade" id={modalId} data-backdrop="static" data-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="staticBackdropLabel">{header}</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">{body}</div>
                <div className="modal-footer">
                    {modalFooter}
                </div>
                </div>
            </div>
        </div>
    );
};

export default Modal;