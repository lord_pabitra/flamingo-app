import './pre-footer.css';

const PreFooter = () => {
    return (
		<div className="row mx-0 pre-footer-outer">
			<div className="col-sm-12 col-md-12 col-lg-9 col-xl-8 pre-footer-wrapper">
				<div className="vc_column-inner">
					<div className="wpb_wrapper">
						<div className="row">
							<div id="contact_us_section" className="col-sm-12 col-lg-6 col-md-6 px-0">
								<div className="vc_column-inner vc-custom-1">
									<div className="wpb_wrapper">
										<div style={{height: '20px'}}><span className="vc_empty_space_inner"></span></div>
										<div className="wpb_raw_code wpb_content_element wpb_raw_html">
											<div className="wpb_wrapper">
												<div className="widget">
													<h2 className="widgettitle">Contact Us</h2>
													<ul className="general-style2">
														<li>
															<strong><i className="fa fa-map-marker" aria-hidden="true"></i>Location</strong>
															<p>121 Park Drive, Varick Str, Newyork, USA</p>
														</li>
														<li>
															<strong><i className="fa fa-paper-plane" aria-hidden="true"></i>Email us</strong>
															<p><a href="mailto:contact@example.com">contact@example.com</a></p>
														</li>
														<li>
															<strong><i className="fa fa-phone" aria-hidden="true"></i> Phone</strong>
															<p>+32-456-789012</p>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<div className="vc_empty_space  d-none d-md-block" style={{height: '60px'}}><span className="vc_empty_space_inner"></span></div>
									</div>
								</div>
							</div>
							<div className="col-sm-12 col-lg-6 col-md-6 px-0">
								<div className="vc_column-inner vc-custom-2">
									<div className="wpb_wrapper">
										<div style={{height: '20px'}}><span className="vc_empty_space_inner"></span></div>
										<div className="vc_wp_posts wpb_content_element">
											<div className="widget widget_recent_entries">
												<h2 className="widgettitle">Recent Posts</h2>
												<ul>
													<li> <a href="http://demo2.themelexus.com/corpec/2018/05/28/decisions-are-the-frequent-fabric-of-our-daily-design/">Decisions are the frequent  fabric of our daily design.</a> <span className="post-date">May 28, 2018</span></li>
													<li> <a href="http://demo2.themelexus.com/corpec/2018/05/28/design-is-inherently-optimistic-that-is-its-power-2/">Design is inherently optimistic. That is  its power.</a> <span className="post-date">May 28, 2018</span></li>
													<li> <a href="http://demo2.themelexus.com/corpec/2018/05/28/falsely-accused-of-committing-a-crime/">Falsely Accused Of Committing A Crime?</a> <span className="post-date">May 28, 2018</span></li>
												</ul>
											</div>
										</div>
										<div style={{height: '20px'}}><span className="vc_empty_space_inner"></span></div>
									</div>
								</div>
							</div>
						</div>
						<div className="vc_separator d-none d-md-block">
							<span className="vc_sep_holder">
								<span className="vc_sep_line" style={{'borderColor': '#e1e1e1'}}></span>
							</span>
						</div>
						<div className="row">
							<div className="col-sm-12 col-lg-6 col-md-6 px-0">
								<div className="vc_column-inner vc-custom-1">
									<div className="wpb_wrapper">
										<div className="d-none d-md-block" style={{height: '80px'}}><span className="vc_empty_space_inner"></span></div>
										<div className="vc_wp_categories wpb_content_element">
											<div className="widget widget_categories">
												<h2 className="widgettitle">Link</h2>
												<ul>
													<li className="cat-item cat-item-16"><a href="http://demo2.themelexus.com/corpec/category/blog/">Blog</a></li>
													<li className="cat-item cat-item-17"><a href="http://demo2.themelexus.com/corpec/category/business/">Business</a></li>
													<li className="cat-item cat-item-18"><a href="http://demo2.themelexus.com/corpec/category/creativity/">Creativity</a></li>
													<li className="cat-item cat-item-19"><a href="http://demo2.themelexus.com/corpec/category/life-lessons/">Life Lessons</a></li>
													<li className="cat-item cat-item-20"><a href="http://demo2.themelexus.com/corpec/category/startup/">Startup</a></li>
													<li className="cat-item cat-item-21"><a href="http://demo2.themelexus.com/corpec/category/tech/">Tech</a></li>
												</ul>
											</div>
										</div>
										<div className="vc_empty_space" style={{height: '80px'}}><span className="vc_empty_space_inner"></span></div>
									</div>
								</div>
							</div>
							<div className="col-sm-12 col-lg-6 col-md-6 col-sm-offset-0 px-0">
								<div className="vc_column-inner vc-custom-2 text-left">
									<div className="wpb_wrapper widget">
										<div className="d-none d-md-block" style={{height: '80px'}}><span className="vc_empty_space_inner"></span></div>
										<h5 className="widgettitle">Newsletter</h5>
										<div>
											<div className="wpb_wrapper">
												<p>Subscribe and get lattest news &amp; update</p>
											</div>
										</div>
										<div role="form" className="wpcf7" id="wpcf7-f5-o1" lang="en-US" dir="ltr">
											<div className="screen-reader-response">
												<p role="status" aria-live="polite" aria-atomic="true"></p>
												<ul></ul>
											</div>
											<form action="/corpec/#wpcf7-f5-o1" method="post" className="wpcf7-form init" noValidate="novalidate">
												<div>
												<span className="wpcf7-form-control-wrap email-765">
													<input type="email" name="email-765" size="40" className="wpcf7-form-control" aria-required="true" placeholder="Your Email"/>
												</span>
												</div>
												<p>
													<input type="submit" value="subscribe" className="wpcf7-form-control wpcf7-submit"/>
													<span className="ajax-loader"></span>
												</p>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    );
};

export default PreFooter;